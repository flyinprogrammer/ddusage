package main

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-resty/resty"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

const (
	DDAPIKey = "DD_API_KEY"
	DDAppKey = "DD_APP_KEY"
	ESURL    = "ES_URL"
	ESIndex  = "ddusage"
	ESType   = "host"
)

func main() {
	apiKey, ok := os.LookupEnv(DDAPIKey)
	if !ok {
		log.Fatal(DDAPIKey + " env not set. go get an api key")
	}

	appKey, ok := os.LookupEnv(DDAppKey)
	if !ok {
		log.Fatal(DDAppKey + " env not set. go get an app key")
	}

	esURL, ok := os.LookupEnv(ESURL)
	if !ok {
		log.Fatal(ESURL + " env not set. go setup an es cluster")
	}
	client, err := elastic.NewClient(elastic.SetURL(esURL), elastic.SetSniff(false))
	if err != nil {
		log.Fatal(err)
	}

	startCron(client, apiKey, appKey)

}

func startCron(c *elastic.Client, apiKey string, appKey string) {
	t := time.NewTicker(10 * time.Minute)
	for {
		infSummary, err := getInfrastructureSummary(apiKey, appKey)
		if err != nil {
			log.WithError(err).Error("could not get hosts back")
		} else {
			upsert(c, infSummary)
		}
		<-t.C
	}
}

func upsert(c *elastic.Client, infSummary *InfrastructureSummary) {
	bulk := c.Bulk().Index(ESIndex).Type(ESType)
	count := 0
	for _, h := range infSummary.Rows {
		data := h.toESHost()
		bulk.Add(elastic.NewBulkUpdateRequest().Id(data.ID).Doc(data).DocAsUpsert(true))
		if bulk.NumberOfActions() >= 50 {
			log.WithField("count", count).Info("pushing hosts")
			_, err := bulk.Do(context.TODO())
			if err != nil {
				log.WithError(err).Error("error upserting docs")
				return
			}
			count += 50
		}
	}

	if bulk.NumberOfActions() > 0 {
		log.Info("doing last push")
		_, err := bulk.Do(context.TODO())
		if err != nil {
			log.WithError(err).Error("error upserting docs")
			return
		}
	}
	log.WithField("count", len(infSummary.Rows)).Info("updated es")
}

type InfrastructureSummary struct {
	Rows []Host `json:"rows"`
}

type Host struct {
	Apps         []string            `json:"apps"`
	DisplayName  string              `json:"display_name"`
	HostName     string              `json:"host_name"`
	ID           int64               `json:"id"`
	LastSeen     int64               `json:"last_seen"`
	Name         string              `json:"name"`
	TagsBySource map[string][]string `json:"tags_by_source"`
	Up           bool                `json:"up"`
	AWSName      string              `json:"aws_name"`
}

type ESHost struct {
	Apps     []string          `json:"apps"`
	ID       string            `json:"id"`
	LastSeen time.Time         `json:"last_seen"`
	Names    []string          `json:"names"`
	Tags     map[string]string `json:"tags"`
	Up       bool              `json:"up"`
	Sources  []string          `json:"sources"`
}

func (h *Host) toESHost() ESHost {
	return ESHost{
		Apps:     h.Apps,
		ID:       h.idString(),
		LastSeen: h.lastSeenTime(),
		Names:    h.names(),
		Tags:     h.tags(),
		Up:       h.Up,
		Sources:  h.sources(),
	}
}

func (h *Host) names() []string {
	return []string{
		h.DisplayName,
		h.Name,
		h.HostName,
		h.AWSName,
	}
}

func (h *Host) idString() string {
	return strconv.FormatInt(h.ID, 10)
}

func (h *Host) lastSeenTime() (t time.Time) {
	t = time.Unix(h.LastSeen, 0)
	return
}

func (h *Host) tags() (tags map[string]string) {
	tags = make(map[string]string)
	for _, rawValues := range h.TagsBySource {
		for _, rV := range rawValues {
			k, v := StringToTag(rV)
			// TODO: it is possible that the k is already set.
			tags[k] = v
		}
	}
	return
}

func StringToTag(semiColonString string) (key, value string) {
	s := CleanseString(semiColonString)
	ary := strings.SplitN(s, ":", 2)
	key = ary[0]
	if len(ary) == 1 {
		value = "no-value"
	} else {
		value = ary[1]
	}
	return
}

func CleanseString(s string) string {
	return strings.ToLower(strings.Replace(s, " ", "", -1))
}

func (h *Host) sources() (sources []string) {
	for k := range h.TagsBySource {
		sources = append(sources, CleanseString(k))
	}
	return
}

func getInfrastructureSummary(apiKey, appKey string) (*InfrastructureSummary, error) {

	resp, err := resty.R().
		SetQueryParams(map[string]string{
			"api_key":         apiKey,
			"application_key": appKey,
			"with_apps":       "true",
			"with_source":     "true",
		}).
		Get("https://app.datadoghq.com/reports/v2/overview")
	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, errors.New("did not get a 200 back from the api")
	}

	var theHosts InfrastructureSummary
	if err := json.Unmarshal(resp.Body(), &theHosts); err != nil {
		return nil, err
	}

	return &theHosts, nil
}
